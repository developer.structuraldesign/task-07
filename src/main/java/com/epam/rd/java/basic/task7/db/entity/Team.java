package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class Team {

	private int id;

	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam(String name) {
		Team result = new Team();
		result.setName(name);
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Team team = (Team) o;
		return id == team.id && name.equals(team.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(/*id,*/ name);
	}

	@Override
	public String toString() {
		return name;
	}
}
