package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;


public class DBManager {

	private static final String CONNECTION_URL = "jdbc:derby:memory:test2db;create=true";
//	private static final String CONNECTION_URL = "jdbc:mysql://localhost:3306/?user=Glebbi4&password=zhelezo";
	private static final String SHUTDOWN_URL = "jdbc:derby:;shutdown=true";
	private static final String APP_PROPS_FILE = "app.properties";
	private static final String APP_CONTENT = "connection.url=" + CONNECTION_URL;
	private static final String DERBY_LOG_FILE = "derby_user.log";

	private static final String QUERY_MAX_ID_USER = "SELECT MAX(id) as max_id from users";
	private static final String QUERY_MAX_ID_TEAM = "SELECT MAX(id) as max_id from teams";

	private Connection conn;
	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
			try {
				instance.initConnection();
			} catch (SQLException | IOException e) {
				e.printStackTrace();
				return null;
			}
		}
		return instance;
	}

	private DBManager() {
	}

	private void initConnection() throws SQLException, IOException {
		var config = Files.readString(Path.of(APP_PROPS_FILE));
		var connectionUrl = config.substring(config.indexOf("=") + 1);
		conn = DriverManager.getConnection(connectionUrl);
	}

	public List<User> findAllUsers() throws DBException {
		List<User> result = null;
		String query = "SELECT id, login FROM users";

		try (PreparedStatement ps = conn.prepareStatement(query); ResultSet rs = ps.executeQuery()) {
			while (rs.next()) {
				if (result == null) {
					result = new LinkedList<>();
				}
				User user = new User();
				user.setId(/*rs.getInt("id")*/ 0);
				user.setLogin(rs.getString("login"));
				result.add(user);
			}
		} catch (SQLException e) {
			throw new DBException("Error finding all users!", e.getCause());
		}

		return result;
	}

	public boolean insertUser(User user) throws DBException {
		if (user == null || user.getLogin() == null) {
			return false;
		}
		boolean result;
		String query = "INSERT INTO users (id, login) VALUES (DEFAULT, ?)";
		try {
			boolean commitFlag;
			commitFlag = conn.getAutoCommit();
			conn.setAutoCommit(false);

			try (PreparedStatement ps = conn.prepareStatement(query)) {
				int pos = 1;
				ps.setString(pos++, user.getLogin());
				result = !ps.execute();
				conn.commit();
				conn.setAutoCommit(commitFlag);
			} catch (SQLException e) {
				conn.rollback();
				throw new DBException("Error insert user!", e.getCause());
			}
		} catch (SQLException ex) {
			throw new DBException("Error creating commit!", ex.getCause());
		}

		return result;
	}

	public boolean deleteUsers(User... users) throws DBException {
		if (users == null) {
			return false;
		}
		boolean result = false;
		String query = "DELETE FROM users WHERE login LIKE ?";
		try {
			boolean commitFlag;
			commitFlag = conn.getAutoCommit();
			conn.setAutoCommit(false);

			for (User user : users) {
				try (PreparedStatement ps = conn.prepareStatement(query)) {
					int pos = 1;
					ps.setString(pos++, user.getLogin());
					result = !ps.execute();
				} catch (SQLException e) {
					throw new SQLException(e);
				}
			}

			conn.commit();
			conn.setAutoCommit(commitFlag);
		} catch (SQLException ex) {
			try {
				conn.rollback();
			} catch (SQLException ignored) {}
			throw new DBException("Error creating commit!", ex.getCause());
		}

		return result;
	}

	public User getUser(String login) throws DBException {
		if (login == null) {
			return null;
		}

		User result = null;
		ResultSet rs = null;
		String query = "SELECT id, login FROM users WHERE login LIKE ?";

		try (PreparedStatement ps = conn.prepareStatement(query)) {
			int pos = 1;
			ps.setString(pos++, login);
			rs = ps.executeQuery();
			if (rs.next()) {
				result = new User();
				result.setId(rs.getInt("id"));
				result.setLogin(rs.getString("login"));
			}
		} catch (SQLException ex) {
			throw new DBException("Getting User error!", ex.getCause());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException ignored) {}
			}
		}

		return result;
	}

	public Team getTeam(String name) throws DBException {
		if (name == null) {
			return null;
		}

		Team result = null;
		ResultSet rs = null;
		String query = "SELECT id, name FROM teams WHERE name LIKE ?";

		try (PreparedStatement ps = conn.prepareStatement(query)) {
			int pos = 1;
			ps.setString(pos++, name);
			rs = ps.executeQuery();
			if (rs.next()) {
				result = new Team();
				result.setId(rs.getInt("id"));
				result.setName(rs.getString("name"));
			}
		} catch (SQLException ex) {
			throw new DBException("Getting Team error!", ex.getCause());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException ignored) {}
			}
		}

		return result;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> result = null;
		String query = "SELECT id, name FROM teams";

		try (PreparedStatement ps = conn.prepareStatement(query); ResultSet rs = ps.executeQuery()) {
			while (rs.next()) {
				if (result == null) {
					result = new LinkedList<>();
				}
				Team team = new Team();
				team.setId(/*rs.getInt("id")*/ 0);
				team.setName(rs.getString("name"));
				result.add(team);
			}
		} catch (SQLException e) {
			throw new DBException("Error finding all teams!", e.getCause());
		}

		return result;
	}

	public boolean insertTeam(Team team) throws DBException {
		if (team == null || team.getName() == null) {
			return false;
		}

		boolean result;
		try {
			boolean commitFlag;
			commitFlag = conn.getAutoCommit();
			conn.setAutoCommit(false);
			String query = "INSERT INTO teams (id, name) VALUES (DEFAULT , ?)";
			try (PreparedStatement ps = conn.prepareStatement(query)) {
				int pos = 1;
				ps.setString(pos++, team.getName());
				result = !ps.execute();
				conn.commit();
				conn.setAutoCommit(commitFlag);
			} catch (SQLException e) {
				conn.rollback();
				throw new DBException("Error insert team!", e.getCause());
			}
		} catch (SQLException ex) {
			throw new DBException("Error creating commit!", ex.getCause());
		}

		return result;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		if (user == null || teams == null || teams.length == 0) {
			return false;
		}

		fillUserId(user);
		fillTeamsId(teams);

		boolean result = false;
		try {
			boolean commitFlag;
			commitFlag = conn.getAutoCommit();
			conn.setAutoCommit(false);

			String query = "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";
			for (Team team : teams) {
				try (PreparedStatement ps = conn.prepareStatement(query)) {
					int pos = 1;
					ps.setInt(pos++, user.getId());
					ps.setInt(pos++, team.getId());
					result = !ps.execute();
				} catch (SQLException e) {
//					throw new DBException("Error insert team!", e.getCause());
					throw new SQLException(e);
				}
			}

			conn.commit();
			conn.setAutoCommit(commitFlag);
		} catch (SQLException ex) {
			try {
				conn.rollback();
			} catch (SQLException ignored) {}
			throw new DBException("Error creating commit!", ex.getCause());
		}

		return result;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		if (user == null) {
			return null;
		}

		fillUserId(user);
		List<Team> result = new LinkedList<>();
		String query = "SELECT id, name FROM teams WHERE id IN (SELECT team_id FROM users_teams WHERE user_id = ?) ";
		ResultSet rs = null;

		try (PreparedStatement ps = conn.prepareStatement(query)) {
			int pos = 1;
			ps.setInt(pos++, user.getId());
			rs = ps.executeQuery();
			while (rs.next()) {
				Team team = new Team();
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));
				result.add(team);
			}
		} catch (SQLException e) {
			throw new DBException("Error getUserTeams!", e.getCause());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException ignored) {}
			}
		}

		return result;
	}

	public boolean deleteTeam(Team team) throws DBException {
		if (team == null) {
			return false;
		}

		boolean result;
		String query = "DELETE FROM teams WHERE name LIKE ?";
		try {
			boolean commitFlag;
			commitFlag = conn.getAutoCommit();
			conn.setAutoCommit(false);

			try (PreparedStatement ps = conn.prepareStatement(query)) {
				int pos = 1;
				ps.setString(pos++, team.getName());
				result = !ps.execute();
			} catch (SQLException e) {
				throw new SQLException(e);
			}

			conn.commit();
			conn.setAutoCommit(commitFlag);
		} catch (SQLException ex) {
			try {
				conn.rollback();
			} catch (SQLException ignored) {}
			throw new DBException("Error creating commit!", ex.getCause());
		}

		return result;
	}

	public boolean updateTeam(Team team) throws DBException {
		if (team == null) {
			return false;
		}

		boolean result;
		String query = "UPDATE teams SET name = ? WHERE id = ?";

		try {
			boolean commitFlag;
			commitFlag = conn.getAutoCommit();
			conn.setAutoCommit(false);

			try (PreparedStatement ps = conn.prepareStatement(query)) {
				int pos = 1;
				ps.setString(pos++, team.getName());
				ps.setInt(pos++, team.getId());
				result = ps.execute();
			} catch (SQLException e) {
				throw new SQLException(e);
			}

			conn.commit();
			conn.setAutoCommit(commitFlag);
		} catch (SQLException ex) {
			try {
				conn.rollback();
			} catch (SQLException ignored) {}
			throw new DBException("Error creating commit!", ex.getCause());
		}

		return result;
	}

	private void fillUserId(User user) throws DBException {
		if (user == null || user.getLogin() == null) {
			return;
		}

		ResultSet rs = null;
		String query = "SELECT id, login FROM users WHERE login LIKE ?";

		try (PreparedStatement ps = conn.prepareStatement(query)) {
			int pos = 1;
			ps.setString(pos++, user.getLogin());
			rs = ps.executeQuery();
			if (rs.next()) {
				user.setId(rs.getInt("id"));
				user.setLogin(rs.getString("login"));
			}
		} catch (SQLException ex) {
			throw new DBException("Getting User error!", ex.getCause());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException ignored) {}
			}
		}
	}

	private void fillTeamsId(Team... teams) throws DBException {
		if (teams == null || teams.length == 0) {
			return;
		}

		String query = "SELECT id, name FROM teams WHERE name IN (?)";
		String preparedQuery = fillPlaceHolders(query,
				Arrays.stream(teams).map(Team::getName).collect(Collectors.toList()));

		try (PreparedStatement ps = conn.prepareStatement(preparedQuery); ResultSet rs = ps.executeQuery()) {
			while (rs.next()) {
				Team temp = new Team();
				temp.setId(rs.getInt("id"));
				temp.setName(rs.getString("name"));
				for (Team team : teams) {
					if (team.getName().equals(temp.getName()) && team.getId() == 0) {
						team.setId(temp.getId());
					}
				}
			}
		} catch (SQLException e) {
			throw new DBException("Error finding all teams!", e.getCause());
		}
	}

	private static String fillPlaceHolders(final String templateString, final List<String> values){
		if (templateString == null || values == null || values.size() == 0) return null;

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < values.size(); ++i) {
			sb.append("'");
			sb.append(values.get(i));
			sb.append("'");
			if (i != values.size() - 1){
				sb.append(',');
			}
		}

		String Result = templateString.substring(0, templateString.indexOf('?')) + sb.toString()
				+ templateString.substring(templateString.indexOf('?') + 1);

		return Result;
	}

	private int getMaxId(String query) throws DBException {
		int result = -1;
		try (PreparedStatement ps = conn.prepareStatement(query); ResultSet rs = ps.executeQuery()) {
			while (rs.next()) {
				result = rs.getInt("max_id");
			}
		} catch (SQLException e) {
			throw new DBException("Error finding all users!", e.getCause());
		}
		return result;
	}
}
